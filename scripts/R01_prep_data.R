library(data.table)
library(matrixStats)
# global variables: Totalnttype, Outputdir

## TODO: avoid copy matrix at any time to save memory

ddmread_j <-function(fileinfo, j, varlist = NULL){
  readfile <- gsub("XXX", j, fileinfo[["file"]])
  ddm_j <- fread(readfile, header=TRUE, na.strings=".", colClasses= fileinfo[["coltype"]], select=varlist) # working in data.table 1.9.6
  #ddm_j <- fread(afile, header=TRUE, na.strings=".", select=varlist) # working in data.table > 1.10
  return(ddm_j)
}

ddmread <- function(afileinfo, yfileinfo, selectvars, selectmuttype, readinvars = NULL){
  matrixlist <- list()
  selectvars <- selectvars[selectvars !="nttypecode"] # nttypecode will always be included by reading data by type
  for (j in 1:Totalnttype){
    dataall <- ddmread_j(afileinfo, j, varlist = readinvars)
    yall <- ddmread_j(yfileinfo, j)
    dataall[,y:=yall]
    dataselect <- dataall[eval(parse(text =selectmuttype))]
    anno <- dataselect[ , selectvars, with = F]
    y <- dataselect[ , "y", with = F]
    genename <- dataselect[ , "genename", with = F]
    matrixlist[[j]] <- list(anno, y, genename)
  }
  return(matrixlist)
}

mlcoluniq <- function(matrixlist, checkcols = c("functypecode")){
  # check if all column in one dt has at least two values, if not, stop script
  # check if all column in different lists have the same set of unique values
  uniqvlist <- list()
  for (j in 1:Totalnttype){
    anno <- matrixlist[[j]][[1]]
    uniq <- lapply(anno, unique)
    uniqvlist[[j]] <- lapply(uniq, sort, na.last = NA)
    uniqc <- lapply(uniq, function (x) length(x[!is.na(x)]) )
    if (length(uniq[uniqc < 2]) > 0){
      badcol < names(uniq[uniqc < 2])
    stop(paste(c("the annotation data has only one value:", badcol, "nttype", j), collapse =" "))
    }
  }
  for (c in checkcols){
    if (length(unique(lapply(uniqvlist, `[[`, c))) > 1) stop(paste( "col", c, "have different values in different nttype"))
    print(paste(c("col",c, unlist(unique(lapply(uniqvlist, `[[`, c)))), collapse = " "))
  }
}

dtcoldup <- function(dt){
  # check if any column is duplicate, if yes, stop script
  # Note this is a rough way (check sum)
  colsum <- lapply(dt, sum , na.rm = TRUE)
  colsum <- unlist(colsum)
  if (length(unique(colsum)) < length(colsum)){
    dname <- names(colsum[colsum %in% unique(colsum[duplicated(colsum)])])
    stop(paste(c("duplicated columns found", dname), collapse = " "))
  }
}

ddmcode <- function(matrixlist, selectvars, functypecodelevel = NULL){
  # add intercept, if have functtypecode, then code and move to the front.
  selectvars <- selectvars[selectvars !="nttypecode"]
  print("coding...")
  mlcoluniq(matrixlist)
  for (j in 1:Totalnttype){
    anno <- matrixlist[[j]][[1]]
    if ("functypecode" %in% selectvars){
      if (!is.null(functypecodelevel)) anno[,functypecode:= relevel(as.factor(anno[["functypecode"]]), ref = functypecodelevel)]
      newfunctype <- model.matrix( ~ functypecode, model.frame(~ functypecode, anno, na.action=na.pass ))
      anno[,functypecode:=NULL]
      newanno <- data.table(newfunctype)
    } else {
      newanno <- data.table("(Intercept)"=rep(1,dim(anno)[1]))
    }
    newanno[, colnames(anno):=anno]
    dtcoldup(newanno)
    matrixlist[[j]][[1]] <- newanno
  }
  return(matrixlist)
}

ddmprocess <- function(matrixlist, qnvars = c("expr","repl","hic"), qnvarimpute=c(-1.8,0.3)){
  # for vars in qnvars, will plug in normal distributed values for missing values using a normal distribution with width of qnvarimpute[2]  and a downshift of the mean by qnvarimpute[1] compared to distribution of all.
  # for others, should all be categorical and will assign the lowest level for missing values and then normalize.
  print("processing ...")
  for (j in 1:Totalnttype){
    anno <- matrixlist[[j]][[1]]
    innames <- colnames(anno)
    inqnvars <- innames[innames %in% qnvars]
    cvars <- innames[!(innames %in% qnvars)]
    cvars <- cvars[!(cvars %in% c("(Intercept)", "chrom","start","end","ref","alt"))]
    if (length(inqnvars) > 0){
      genename <- matrixlist[[j]][[3]]
      for (qnvar in inqnvars){
         NAgns <- unique(genename[which(is.na(anno[[qnvar]]))])[["genename"]]
         vmean <- if (qnvar != "repl") qnvarimpute[1] else -qnvarimpute[1]
         v <- rnorm(length(NAgns), vmean, qnvarimpute[2]) # if positions share same genename, use same inputed value
         NAgnc <- table(genename[which(is.na(anno[[qnvar]]))])
         vall <- rep(v, NAgnc[NAgns])
         set(anno, which(is.na(anno[[qnvar]])), qnvar, vall)
      }
    }
    if (length(cvars) > 0) {
      for (cvar in cvars){
        cv <- min(anno[[cvar]], na.rm = T)
        set(anno, which(is.na(anno[[cvar]])), j = cvar, value = cv)
      }
    }
    matrixlist[[j]][[1]] <- anno[complete.cases(anno),]
    matrixlist[[j]][[2]] <- matrixlist[[j]][[2]][complete.cases(anno),]
    matrixlist[[j]][[3]] <- matrixlist[[j]][[3]][complete.cases(anno),]
    rm(anno, genename);gc()
  }
  allsum <- colSums(do.call(rbind,lapply(matrixlist, function(x) x[[1]][,lapply(.SD,sum)])))
  alllen <-  colSums(do.call(rbind,lapply(matrixlist, function(x) dim(x[[1]]))))[1]
  allmu <- allsum/alllen
  allvarlist <- list()
  for (j in 1:Totalnttype){
    allvarlist[[j]] <- colVars(matrixlist[[j]][[1]],center=allmu) * dim(matrixlist[[j]][[1]])[1]
    gc()
  }
  allsd <- sqrt(colSums(do.call(rbind, allvarlist))/(alllen-1))
  save(allmu, allsd, file=paste(Outputdir,"colmu_sd_funcv.Rdata", sep=""))
  for (j in 1:Totalnttype){
    print("normalizing categorical variables in annotation matrix ...")
    anno <- matrixlist[[j]][[1]]
    for (cvar in cvars) set(anno, j = cvar, value = (anno[[cvar]] - allmu[cvar])/allsd[cvar])
    gc()
  }
  return(matrixlist)
}

splitddm <- function(matrixlist, cpgenelist){
# divide matrixlist into groups defined in cpgenelist, if not in any group in cpggenelist, it is put in the last group. need to optimize almost hit 20G
  outmatrixlist <- vector("list", length(cpgenelist)+1)
  for (j in 1:Totalnttype){
    annoall <- matrixlist[[j]][[1]]
    yall <- matrixlist[[j]][[2]]
    geneall <- matrixlist[[j]][[3]]
    for (m in 1:length(outmatrixlist)) {
      if (m != length(outmatrixlist)) {
        anno <- annoall[geneall[["genename"]] %in% cpgenelist[[m]]]
        y <- yall[geneall[["genename"]] %in% cpgenelist[[m]]]
        gene <- geneall[genename %in% cpgenelist[[m]]]
      } else {
        anno <- annoall[!(geneall[["genename"]] %in% as.matrix(unlist(cpgenelist)))]
        y <- yall[!(geneall[["genename"]] %in% as.matrix(unlist(cpgenelist)))]
        gene <- geneall[!(genename %in% as.matrix(unlist(cpgenelist)))]
      }
      outmatrixlist[[m]][[j]] <- list(anno, y,gene)
      gc()
    }
  }
  return(outmatrixlist)
}

readmodeldata <- function(afileinfo, yfileinfo, selectvars, selectmuttype, readinvars = NULL,qnvars = c("expr","repl","hic"),functypecodelevel = NULL,qnvarimpute=c(-1.8,0.3)){
  rawmlist <- ddmread(afileinfo, yfileinfo, selectvars, selectmuttype, readinvars)
  rawmlist_code <- ddmcode(rawmlist, selectvars, functypecodelevel)
  rm(rawmlist); gc()
  matrixlist <- ddmprocess(rawmlist_code, qnvars,qnvarimpute)
  return(matrixlist)
}

