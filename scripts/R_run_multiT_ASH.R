print("running ASH, currently can only be run locally")
print(Sys.time())

source(commandArgs(TRUE)[1])
#commandArgs <- function(x) c("LUSC","LUSC")

#for (CVindex in 1:53){
# source(paste0("/Volumes/cancer_somatic/data_run/combined_20170526_5_10CV/CV",CVindex,"/CV",CVindex,"_2017-10-25_config.R"))

load(paste(Outputdirbase, Tlist[1],"/", Tlist[1],"_parameters_","_funcv.Rdata",sep=""))
Vlist <- names(funcvpars[[1]]$fixed[!funcvpars[[1]]$fixed])

parm <- getpar(Outputdirbase, Tlist)
scaleall <- getscale(Outputdirbase, Tlist)
muall <- scaleall[[1]]
sdall <- scaleall[[2]]
# sd across all tumors are very similar, so no need to scale
# TODO: may need to scale-ASH-rescale in the future
ASH_par(parm, Tlist, Vlist, Outputdirbase)
#}

#for (CVindex in 1:53){
#  source(paste0("/Volumes/cancer_somatic/data_run/combined_20170526_5_10CV/CV",CVindex,"/CV",CVindex,"_2017-10-25_config.R"))
  
temppars <- list()
for (ttype in Tlist){
	hmmfile <- paste(Outputdirbase,"/",ttype,"/",ttype,"_hmmOC_ASHmean.rds", sep="")
	if (file.exists(hmmfile)){
	print(ttype)
  temppars[[hmmfile]] <- getpars(readRDS(hmmfile))}
	print(temppars[[hmmfile]])
}
temppars <- do.call(rbind, temppars)
saveRDS(apply(temppars, 2,mean), paste(Outputdirbase,"/hmmpars_mean.rds", sep=""))
#}

# # leave one-out to get ASHmean
# for (Ttype in Tlist){
#   Tlist1 <- Tlist[Tlist!=Ttype]
#   parm <- getpar(Outputdirbase, Tlist1)
#   ASH_par(parm, Tlist1, Vlist, paste0(Outputdirbase,"/",Ttype,"/",Ttype))
# }
# 
# # leave one out to get HMM parameters
# # note: more stringently, should re-estimate HMM parameters for each new leave-one-out ASHmean, but since ASHmean is very similar
# # so just used the hmmpars estimated invidually.
# for (Ttype in Tlist){
#   temppars <- list()
#   Tlist1 <- Tlist[Tlist!=Ttype]
#   for (ttype in Tlist1){
#     hmmfile <- paste(Outputdirbase,"/",ttype,"/",ttype,"_hmmOC_ASHmean.rds", sep="")
#     if (file.exists(hmmfile))
#       temppars[[hmmfile]] <- getpars(readRDS(hmmfile))
#   }
#   temppars <- do.call(rbind, temppars)
#   saveRDS(apply(temppars, 2,mean), paste(Outputdirbase,"/",Ttype,"/",Ttype,"hmmpars_mean.rds", sep=""))
# }