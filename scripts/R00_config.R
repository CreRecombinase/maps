codedir <- snakemake@config[["scriptdir"]]
Adirbase <- snakemake@config[["annodir"]]
Outputdirbase <- snakemake@config[["outputdir"]]
paramdir <- snakemake@config[["paramdir"]]

Ttype <- snakemake@wildcards[["tumorname"]]
Outputdir <- paste0(Outputdirbase, Ttype, "/")
Ydirbase <- paste0(Outputdir, "data_ycol/")

Totalnttype <- snakemake@nNT

source(paste(codedir,'R00_tools.r',sep=""))
source(paste(codedir,'R00_myfunc.R', sep=""))
source(paste(codedir,'R01_prep_data.R',sep=""))
source(paste(codedir,'R02_BMR_gsQC.R',sep=""))
source(paste(codedir,'R03_model_GMR.R',sep=""))
source(paste(codedir,'R03_model_GMR_HMM.R',sep=""))
source(paste(codedir,'R04_multiT_FDR_siggene.R',sep=""))
source(paste(codedir,'R05_multiT_ASH.R',sep=""))
source(paste(codedir,'R06_multiT_pi0.R',sep=""))
source(paste(codedir,'R00_multiT_get.R',sep=""))


#--- Ttype specific annotation data ---
print(paste("processing tumor", Ttype))
Afileinfo <- list(file = paste(Adirbase, "nttypeXXX_annodata.txt", sep=""),
                  header = c("chrom","start","end","ref","alt","genename","functypecode","nttypecode","expr","repl","hic","mycons","sift","phylop100","MA","ssp","wggerp"),
                  coltype = c("character","numeric","numeric","character","character","character","factor","factor","numeric","numeric","numeric","numeric","numeric","numeric","numeric","numeric","numeric"))

Yfileinfo <- list(file = paste(Ydirbase, Ttype, "_nttypeXXX_y.txt", sep=""),
                 header = c("y"),
                 coltype = c("numeric"))

#--- run parameters ---
Measurevar <- "y"
BMvars <- c("nttypecode", "expr", "repl", "hic")
BMmuttype <- "functypecode == 6 & ssp == 0"
Funcvars <- c("functypecode", "mycons", "sift", "phylop100", "MA")
Functypecodelevel <- "7"
Funcvmuttype <- "functypecode == 7 | functypecode == 8 | (functypecode == 6 & ssp == 1)"
Readinvars <- c("genename", "ssp", BMvars, Funcvars) # This is optional, if not given, then will read all columns
Qnvars = c("expr","repl","hic") # all the rest will be normalized, except for nttypecode
Alpha0 <- 2 # start value for alpha
Beta0 <- 0 # start value for beta
Fixparslist <- list("functypecode6" = c(-0.07292809, -0.09149377, 0.02213031)) # this is for funcvpars, 6vs7 pars

TSG <- readRDS(paste0(paramdir,"TSG.rds"))
OC <- readRDS(paste0(paramdir,"OG.rds"))


#-------------------not used ---------------------------
# dir.create(Outputdir)
# Outputdir <- paste(Outputdirbase, Ttype,"/", sep="")
# Ttype <-commandArgs(T)[2]
# BATCH <- ".B1"
# TSBATCH <- ".TS1.TS2.TS3"
# Outputdirbase <- paste(mainfolder,"/data_run/combined_20170526_5/", sep="")
# Adirbase <-  paste(mainfolder,"/library/annovar_outputs2/",sep="")
# script.dir <- normalizePath(dirname(sys.frame(1)$ofile))
# mainfolderprefix <- strsplit(script.dir,"cancer_somatic")[[1]][1]
# mainfolder <- paste(mainfolderprefix, "cancer_somatic/" , sep ="")
# codefolder <- paste(mainfolder, "cancer_somatic/code/", sep="")
# #--- library files ---
# MutsigCVdir <- paste(mainfolder, "/data_run/combined_20170526_MutsigCV_results", sep="")
# oncodriveFMdir <- paste(mainfolder, "/data_run/combined_20170526_oncodriveFM", sep="")
# oncodriveCLUSTdir <- paste(mainfolder, "/data_run/combined_20170526_oncodriveCLUST", sep="")
# oncodriveFMLdir <- paste(mainfolder, "/data_run/combined_20170526_oncodriveFML", sep="")
# t0t0plusdir <- paste0(mainfolder,"data_run/combined_20170526_2020plus")
# COSMICfile <- paste(mainfolder, '/library/COSMIC_Census_03032016.csv',sep="")
# COSMICfile2 <- paste(mainfolder, '/library/COSMIC_Census_03032016_SNV_only.txt',sep="")
# TSGfile <- paste(mainfolder, '/library/VOGELSTEIN-TSG-list.txt',sep="")
# OCfile <- paste(mainfolder, '/library/VOGELSTEIN-oncogene-list.txt',sep="")
# Pancancerfile <- paste(mainfolder, '/library/TCGA-cancer-gene-list',sep="")
# COSMIC <- read.csv(COSMICfile,head=T)
# COSMIC <- as.matrix(COSMIC$Gene.Symbol)
# COSMIC2 <- as.matrix(read.table(COSMICfile2, header=F))
# TSG <- as.matrix(read.table(TSGfile, header=F))
# OC <- as.matrix(read.table(OCfile, header=F))
# Pancancer <- as.matrix(read.table(Pancancerfile, header=F))
# Cancergene <- unique(c(TSG,OC, Pancancer,COSMIC))
# #multiT parameters:
# Tlist <-c('BLCA', 'BRCA', 'CESC', 'CHOL', 'ESCA', 'GBM', 'LIHC', 'HNSC', 'KICH', 'KIRC', 'KIRP', 'LUAD', 'LUSC', 'PAAD', 'PRAD', 'SARC', 'SKCM', 'TGCT', 'UCEC', 'UCS') # 20
#
# Note: this file is the same as the config file in combined_20170526_3 run except for the outputdirbase. But the script has been changed from commit 8de8766  to commit c0e9ee9 (for background model part, not using qnvarimpute=c(-1.8,0.3) for qnvars but using NA and thus removed when infering BM related parameters.for funcv infer and predict BF, used qnvarimpute=c(0,0))





