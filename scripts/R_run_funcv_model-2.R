print("running R_run_funcv_model.R: tyring to infer parameters for functional variates...using silent mutation data as prior knowledge for gene specific mutation rate paramter
The Difference from main script is that this script didn't infer noncancer parameters")
print(Sys.time())

source(commandArgs(TRUE)[1])

BMparsfile <- paste(Outputdir,"/", Ttype,"_parameters_BMvar.Rdata",sep="")
load(BMparsfile)
print(paste("BMvar parameters loaded from: ", BMparsfile))

Y_mu_gfile <- paste(Outputdir,"/", Ttype,"_BM_y_mu_g.Rdata",sep="")
load(Y_mu_gfile)
print(paste("Y_g_s_all, Mu_g_s_all loaded from: ", Y_mu_gfile))


Matrixlist <- readmodeldata(Afileinfo, c(BMvars,Funcvars), Funcvmuttype, Measurevar, Readinvars , Qnvars,functypecodelevel = Functypecodelevel,qnvarimpute=c(0,0))
Cpgenelist <- list(tsg = TSG, oc =OC)
Matrixlist2<- splitddm(Matrixlist, Cpgenelist)
rm(Matrixlist);gc()

nbeta <- Totalnttype + dim(Matrixlist2[[1]][[1]][[1]])[2] - 1
initpars <- c(rep(Beta0, nbeta), Beta0 ,Alpha0) 
fixstatus <- rep(F, length(initpars))
parsnames <- c(paste("nttype", 1:Totalnttype, sep=""), colnames(Matrixlist2[[1]][[1]][[1]])[2:dim(Matrixlist2[[1]][[1]][[1]])[2]], "beta_f0", "alpha")
names(initpars) <- parsnames
names(fixstatus) <- parsnames
BMparsplugin <- BMpars$fullpars[!BMpars$fixed]
initpars[names(BMparsplugin)] <- BMparsplugin
fixstatus[names(BMparsplugin)] <- T

# plug in previous parameters
load(paste0('/project/mstephens/cancer_somatic/data_run/combined_20170526_5/',Ttype,"/", Ttype,"_parameters_","_funcv.Rdata"))
pluginpars <- funcvpars[[3]]
#----

funcvpars <- list()
for (m in 1:2){
  Cpmatrixlist <- Matrixlist2[[m]]
  for (fixparname in names(Fixparslist)) {
    initpars[fixparname] <- Fixparslist[[fixparname]][m]
    fixstatus[fixparname] <- T
  }
  funcvpars[[m]] <- optifix(initpars, fixstatus, loglikfn, matrixlist= Cpmatrixlist, y_g_s_in=Y_g_s_all, mu_g_s_in=Mu_g_s_all, method = "BFGS", control=list(trace=6, fnscale=-1), hessian=T)
  names(funcvpars[[m]]$fullpars) <- parsnames
}

# plug in previous parameters
funcvpars[[3]] <- pluginpars
#----

# TODO: add $convergence in runlog
Funcvparsfile <- paste(Outputdir,"/", Ttype,"_parameters_","_funcv.Rdata",sep="")
save(funcvpars, file = Funcvparsfile ) # save as .Rd file
Outfuncvparsfile <- paste(Outputdir,"/", Ttype,"_parameters_","_funcv.txt",sep="")
funcvparslist <- lapply(funcvpars,FUN=.parfunc)
outfuncv <- do.call(rbind, funcvparslist)
rownames(outfuncv) <- c(paste("TSG-",Ttype,sep=""), paste("TSG-sd",Ttype,sep=""), paste("OC-",Ttype,sep=""),paste("OC-sd",Ttype,sep=""),paste("nc-",Ttype,sep=""),paste("nc-sd",Ttype,sep=""))
write.table(outfuncv,row.names=T, col.names=F,file=Outfuncvparsfile,sep="\t",quote = F) # save as txt file




