print("running R_run_multiT_FDR_sigene.R, will add FDR to each tumor type")
print(Sys.time())


# commandArgs <- function(x) c("LUSC","LUSC")
# source("~/cancer_somatic/data_run/combined_20170215_1/R00_config_2017-02-17.R")
source(commandArgs(TRUE)[1])
library(dplyr)
library(tidyr)
library(FGEM)

partag <- "_ASHmean_HMM_"
outpartag <- "_ASHmean_HMM_GL_"
GLdir <- paste0(mainfolder,'/library/gene_anno/')
select <- dplyr::select

#filter by blacklis
gdffile <- paste(Outputdir, "/", Ttype, partag, "_gdf.txt", sep="")
gdf <- read.table(gdffile, header=T)
bl <- read.table(paste0(Outputdirbase,"combined20170526_3_blacklist.txt"),header=F)
blgene <- bl[bl[,1]==Ttype,2]
gdf <- gdf[!(gdf$gene %in% blgene),]
oridf <- data.frame(Gene=gdf$gene, post0= 1-gdf$p0, fdr0=gdf$fdr) 

# prepare BF
BFtemp <- gdf$BF
if (length(BFtemp[which(is.na(BFtemp))]) > 0){
  print(paste("in ", ttype, "the following genes' BF are na: they have been changed to 0"))
  print(cbind(gdf$gene[which(is.na(BFtemp))],BFtemp[which(is.na(BFtemp))]))
  BFtemp[which(is.na(BFtemp))] <- 0
}
if (length(BFtemp[which(is.infinite(BFtemp))]) > 0){
  print(paste("in", ttype, "the following genes' BF are inf: they have been changed to 50"))
  print(cbind(gdf$gene[which(is.infinite(BFtemp))],BFtemp[which(is.infinite(BFtemp))]))
  BFtemp[which(is.infinite(BFtemp))] <- 50
}
BFdata <- data.frame(Gene=gdf$gene, BF= exp(BFtemp))

#prepare gene annotation
## CNV
gCNV <- read.table(paste0(GLdir,Ttype, "_tcga_CNV.txt"), header=T)
colnames(gCNV)[1] <- "Gene"
a<-qqnorm(gCNV$gain,plot.it = F)
b<-qqnorm(-gCNV$loss,plot.it = F)
gCNV$gainn <- a$x
gCNV$lossn <- b$x
gCNV$CNsum <- abs(gCNV$gainn-gCNV$lossn)
feature_df <- left_join(BFdata, gCNV[,c("Gene", "CNsum")]) 
## expression
gexpr <- read.table(paste0(GLdir,Ttype, "_tcga_expression_median.txt"), header=T)
colnames(gexpr)[1] <- "Gene"
feature_df <- left_join(feature_df, gexpr[,c("Gene", "exprmean")]) 

## methylation
gmeth <- read.table(paste0(GLdir,Ttype, "_tcga_hm450.txt"), header=T)
colnames(gmeth)[1] <- "Gene"
feature_df <- left_join(feature_df, gmeth[,c("Gene", "methmean")]) 
feature_df <- feature_df[complete.cases(feature_df),]

# fit
fit_df <- FGEM_df(feature_df) #select(fit_df,data) %>% unnest()
saveRDS(fit_df,file=paste0(Outputdir, Ttype,"_GLfit.Rds"))
result_df  <- gen_prior(feature_df,fit_df) %>% inner_join(gen_posterior(feature_df,fit_df))
result_df <- arrange(result_df,desc(posterior))
result_df$fdr <- Posterior.FDR(result_df$posterior)
cadf <- cancer_anno(result_df$Gene)
colnames(cadf)[1] <- "Gene"
outdf <- inner_join(result_df,oridf) %>% inner_join(gCNV) %>% inner_join(cadf)
colnames(outdf)[1] <- "gene"
write.table(outdf , file=paste0(Outputdir, Ttype, outpartag,'_gdf.txt')  , row.names=F, col.names=T, sep="\t", quote = F)
write.table(outdf[outdf$fdr <0.1,] , file=paste0(Outputdir, Ttype, outpartag,'_gdf_siggene.txt')  , row.names=F, col.names=T, sep="\t", quote = F)

temp <- arrange(outdf, fdr0)
print(cancer_frac(outdf$Gene[1:30]))
print(cancer_frac(outdf$Gene[1:50]))
print(cancer_frac(outdf$Gene[1:100]))
print(cancer_frac(temp$Gene[1:30]))
print(cancer_frac(temp$Gene[1:50]))
print(cancer_frac(temp$Gene[1:100]))





