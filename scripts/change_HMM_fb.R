#library(devtools)
#install_github("simingz/hmm_fb")

fb_rcpp <- function(init, A, B, ntimes, return_all = FALSE,homogeneous=TRUE,useC=TRUE,na.allow=TRUE){
  # B is in log scale
  lA <- log(A[1,,])
  nB <- cbind(B[,,1],B[,,2])
  linit <- log(init)
  forward_rcpp(linit, lA, nB, ntimes, return_all=T)
}

viterbi <- function(object,na.allow=TRUE){
  nt <- sum(object@ntimes)
  ns <- object@nstates
  delta <- matrix(nrow=nt,ncol=ns)
  colnames(delta) <- paste("S",1:ns,sep="")
  state <- rep(1,nt)
  delta <- data.frame(state,delta)
  return(delta)
}
assignInNamespace("fb", fb_rcpp,"depmixS4")
assignInNamespace("viterbi", viterbi,"depmixS4")


